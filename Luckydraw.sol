pragma solidity ^0.5.12;

contract Luckydraw {
    address public manager;
    address payable[] public players;
    
    constructor () public{
        manager = msg.sender;
    }

    function enter() public payable{
        require(msg.value >= .01 ether, "PLEASE ENTER MORE THAN .01 ETHER");
        
        players.push(msg.sender);
    }
    
    function random() private view returns (uint){
        return uint(keccak256(abi.encodePacked(block.difficulty, now, players)));
    }
    
    function pickWinner() public{
        require(msg.sender == manager, "ONLY MANAGER");
        
        uint index = random() % players.length;
        players[index].transfer(address(this).balance);
        players = new address payable [](0);
    }
    
    function allplayers() public view returns(address payable[] memory) {
        return players;
    }
    
    function getBalance() public view returns (uint){
        return address(this).balance;
    }
}